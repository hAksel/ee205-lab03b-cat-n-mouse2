#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

echo OK cat, I\'m thinking of a number from 1 to $THE_MAX_VALUE. Make guess:


catGuess=420
while [ $catGuess -ne $THE_NUMBER_IM_THINKING_OF ] 
do
   read catGuess
#   echo "catGues = $catGuess"
   if [ $catGuess -lt 1 ]
      then                       #this normally is on the same indent as if 
      echo "You must enter a number that's >= 1" 
   fi
   
   if [ $catGuess -gt $THE_MAX_VALUE ] 
      then
      echo "You must enter a number that's <= $THE_MAX_VALUE" 
   fi
   
   if [ $catGuess -gt $THE_NUMBER_IM_THINKING_OF ] 
      then 
      if [ $catGuess -lt $THE_MAX_VALUE ] #this nested if statement prevents "you must" & "No cat..."
         then
         echo "No cat... the number I'm thinking of is smaller than $catGuess"
      fi 
   fi

   if [ $catGuess -lt $THE_NUMBER_IM_THINKING_OF ] 
      then
      if [ $catGuess -gt 1 ]
         then
         echo "No cat... the number I'm thinking of is larger than $catGuess" 
      fi
   fi 
done 


echo "You got me." 
echo " .       . "
echo " |\\_---_/|"
echo "/   o_o   \\"
echo "|    U    |"
echo "\\  ._I_.  /"
echo " \`-_____-' "







